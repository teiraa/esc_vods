Georgia's Ranina kicked off its sixth season on 1TV, which means that the 2023 Junior Eurovision season is finally starting! As always, this megathread will serve as a general reference post for national selections, with a schedule, a list of the upcoming livestreams and links to rewatch every single show part of a national final. Feel free to write a comment if you can think of ways to improve the post! Reddit would constantly delete the former post so I had to repost it. :(

As a note, for those curious, The Voice Kids Portugal is included in the thread as in the past two seasons it has been the de-facto national final, despite the lack of an explicit confirmation and it is very likely that RTP will use it this year as well!

[ ](https://pbs.twimg.com/media/FsWIcHLaIAAVqqY?format=jpg&name=4096x4096)

# Previous megathreads

* [Eurovision Song Contest 2022 national finals](https://www.reddit.com/r/eurovision/comments/qj9pb8/vod_links_for_eurovision_2022_national_finals/)
* [Eurovision Young Musicians 2022 national finals](https://www.reddit.com/r/eurovision/comments/uu5zr9/megathread_eurovision_young_musicians_2022/)
* [Junior Eurovision 2022 national finals](https://www.reddit.com/r/eurovision/comments/vea1z4/megathread_junior_eurovision_2022_calendar_of/)
* [Eurovision Song Contest 2023 national finals](https://www.reddit.com/r/eurovision/comments/ysocup/megathread_eurovision_song_contest_2023_calendar/)

*****

# Livestreams

To watch multiple national finals at the same time, or if the links listed here don't work, check out [ESCplus Live](https://live.esc-plus.com)!

* 🇬🇪 **რანინა ● Ranina**

> https://player.1tv.ge/

> https://www.youtube.com/@Ranina-1tv/streams

> https://www.facebook.com/Ranina.1tv.ge/

* 🇵🇹 **The Voice Kids Portugal**

> https://www.rtp.pt/play/direto/rtp1 (available only in Portugal)

> https://www.rtp.pt/play/direto/rtpinternacional

*****

# Schedule

All dates and times are CEST (UTC+2). Click on the timestamps to convert them into your time zone! Some of the rewatch links might become unavailable overtime, feel free to write a comment or a direct message so I can update a link or reupload a show.

|Date and Time|Country|Show|
|:-|:-|:-
|[1 April at 20:00](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230401T22&p1=371) (60 min.)|Georgia|Ranina: Episode 1
|[9 April at 22:22](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230409T2122&p1=133) (147 min.)|Portugal|The Voice Kids: Blind Auditions #1
|[15 April at 19:50](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230415T2150&p1=371) (60 min.)|Georgia|Ranina: Episode 2
|[16 April at 22:15](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230416T2115&p1=133) (165 min.)|Portugal|The Voice Kids: Blind Auditions #2
|[22 April at 20:00](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230422T22&p1=371) (60 min.)|Georgia|Ranina: Episode 3
|[23 April at 22:15](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230423T2115&p1=133) (165 min.)|Portugal|The Voice Kids: Blind Auditions #3
|[29 April at 20:00](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230429T22&p1=371) (60 min.)|Georgia|Ranina: Episode 4
|[30 April at 22:15](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230430T2115&p1=133) (165 min.)|Portugal|The Voice Kids: Blind Auditions #4
|[6 May at 20:00](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230506T22&p1=371) (60 min.)|Georgia|Ranina: Episode 5
|[7 May at 22:15](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230507T2115&p1=133) (165 min.)|Portugal|The Voice Kids: Blind Auditions #5
|[13 May at 20:00](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230513T22&p1=371) (60 min.)|Georgia|Ranina: Episode 6
|[14 May at 22:15](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230514T2115&p1=133) (165 min.)|Portugal|The Voice Kids: Blind Auditions #6
|[20 May at 20:00](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230520T22&p1=371) (60 min.)|Georgia|Ranina: Episode 7
|[21 May at 22:15](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230521T2115&p1=133) (165 min.)|Portugal|The Voice Kids: Battles #1
|[27 May at 20:00](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230527T22&p1=371) (60 min.)|Georgia|Ranina: Episode 8
|[28 May at 22:15](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230528T2115&p1=133) (165 min.)|Portugal|The Voice Kids: Battles #2
|[3 June at 20:00](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230603T22&p1=371) (60 min.)|Georgia|Ranina: Semi-Final
|[4 June at 22:15](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230604T2115&p1=133) (165 min.)|Portugal|The Voice Kids: Semi-Final 1
|[10 June at 20:00](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230610T22&p1=371) (90 min.)|Georgia|**Ranina: Final**
|[11 June at 22:15](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230611T2115&p1=133) (165 min.)|Portugal|The Voice Kids: Semi-Final 2
|[18 June at 22:15](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230618T2115&p1=133) (165 min.)|Portugal|**The Voice Kids: Final**
|5 August|Malta|Malta Junior Eurovision Song Contest: Semi-Final
|12 August|Malta|**Malta Junior Eurovision Song Contest: Final**
|[23 September at 19:25](https://www.timeanddate.com/worldclock/fixedtime.html?iso=20230923T1925&p1=16)|Netherlands|**Junior Songfestival**

*****

# 🇬🇪 Ranina

* Episode 1 (1 April)

> https://player.1tv.ge/1tv/2023-04-01T22:00:00+04:00

> https://www.youtube.com/watch?v=90uw9zJosOo

> https://www.facebook.com/Ranina.1tv.ge/videos/1624250668089735/

* Episode 2 (15 April)
 
> https://player.1tv.ge/1tv/2023-04-15T22:00:00+04:00

> https://www.youtube.com/watch?v=DCDHvHyVj5E

> https://www.facebook.com/Ranina.1tv.ge/videos/247458801018162/

* Episode 3 (22 April)

> https://player.1tv.ge/1tv/2023-04-22T22:00:00+04:00

> https://www.youtube.com/watch?v=-osDaCxDE_I

> https://www.facebook.com/Ranina.1tv.ge/videos/762625492148086/

* Episode 4 (29 April)

> https://player.1tv.ge/1tv/2023-04-29T22:00:00+04:00

> https://youtu.be/MgUaHXIvgPU

> https://www.facebook.com/Ranina.1tv.ge/videos/191136113805112/

* Episode 5 (6 May)

> https://player.1tv.ge/1tv/2023-05-06T22:00:00+04:00

> https://youtu.be/nuGvplt7B2g

> https://www.facebook.com/Ranina.1tv.ge/videos/952086142644997/

* Episode 6 (13 May)

> https://player.1tv.ge/1tv/2023-05-13T21:50:00+04:00

> https://youtu.be/weW_V5DS_Ac

> https://www.facebook.com/Ranina.1tv.ge/videos/974680476878297/

* Episode 7 (20 May)

> https://player.1tv.ge/1tv/2023-05-20T22:00:00+04:00

> https://youtu.be/Sz7pAewU65A

> https://www.facebook.com/Ranina.1tv.ge/videos/2823917524454475/

* Episode 8 (27 May)

> https://player.1tv.ge/1tv/2023-05-27T22:00:00+04:00

> https://youtu.be/ZQVy8KFQLqY

> https://www.facebook.com/Ranina.1tv.ge/videos/179748928379968/

* Semi-Final (3 June)

> https://player.1tv.ge/1tv/2023-06-03T22:00:00+04:00

> https://youtu.be/fKZoXnZJfWY

> https://www.facebook.com/Ranina.1tv.ge/videos/268303999060834/

* Final (10 June)

> https://player.1tv.ge/1tv/2023-06-10T22:00:00+04:00

> https://youtu.be/YWQ5eAqcY0g

> https://www.facebook.com/Ranina.1tv.ge/videos/2422873424550672/

# 🇵🇹 The Voice Kids

* Blind Auditions #1 (9 April)

> https://www.rtp.pt/play/p11585/e684098/the-voice-kids (available only in Portugal)

* Blind Auditions #2 (16 April)

> https://www.rtp.pt/play/p11585/e685551/the-voice-kids (available only in Portugal)

* Blind Auditions #3 (23 April)

> https://www.rtp.pt/play/p11585/e687138/the-voice-kids (available only in Portugal)

* Blind Auditions #4 (30 April)

> https://www.rtp.pt/play/p11585/e688587/the-voice-kids (available only in Portugal)

* Blind Auditions #5 (7 May)

> https://www.rtp.pt/play/p11585/e690077/the-voice-kids (available only in Portugal)

* Blind Auditions #6 (14 May)

> https://www.rtp.pt/play/p11585/e691649/the-voice-kids (available only in Portugal)

* Battles #1 (21 May)

> https://www.rtp.pt/play/p11585/e693336/the-voice-kids (available only in Portugal)

* Battles #2 (28 May)

> https://www.rtp.pt/play/p11585/e694858/the-voice-kids (available only in Portugal)

* Semi-Final 1 (4 June)

> https://www.rtp.pt/play/p11585/e696521/the-voice-kids (available only in Portugal)

* Semi-Final 2 (11 June)

> https://www.rtp.pt/play/p11585/e697994/the-voice-kids (available only in Portugal)
